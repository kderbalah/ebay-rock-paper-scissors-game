# ROCK PAPER SCISSORS
An experimental [Rock–paper–scissors](https://en.wikipedia.org/wiki/Rock%E2%80%93paper%E2%80%93scissors) game for eBay interview!

![picture](https://image.ibb.co/kRkLYK/large_screen.png)

## Game modes
- `Player Vs. Computer`: default game mode.
- `Player Vs. Computer`: The `Plyer` has the ability to let the `Computer` select a random hand sign!
- You can switch between `Player Vs. Computer` and `Computer Vs. Computer` on each round! 

## Browser support (Cross-browser support)
Chrome | Firefox | IE | Opera | Safari  |
--- | --- | --- | --- | --- |
Latest ✔ | Latest ✔ | 11+ ✔ | Latest ✔ | 6.1+ ✔ |

## Responsiveness
The game is responsive on different mobile/tablet screens.

## Test Coverage
86.67%

## Extension
[Developer] The game is extensible (i.e. more hand signs). 
You just need to add more sings and rules to the constant file and you also need to define an image or icon for each.

## Prerequisites

[Node.js](http://nodejs.org/) >= v6 must be installed.

## Installation

- Running `npm install` in the app's root directory will install everything you need for development.

## Development Server

- `npm run dev` will run the app's development server at [http://localhost:3000](http://localhost:3000), automatically reloading the page on every change.

## Running Tests

- `npm test` will run the tests once.
- `npm run test:coverage` will run the tests and produce a coverage report in `coverage/`.
- `npm run test:watch` will run the tests on every change.

## Linting
- `npm run lint` will lint the codebase using ESlint

## Building

- `npm run build` creates a production build by default.

- To create a development build, set the `NODE_ENV` environment variable to `development` while running this command.
   
- [For production] After building the codebase using `npm rn build`, you need an HTTP server to serve the static assets 
i.e. [http-server npm package](https://www.npmjs.com/package/http-server) or you can use docker to run it using `nginx` server (docker file is included)

- `npm run clean` will delete built resources.
