import rockIcon from '../images/rock.svg';
import paperIcon from '../images/paper.svg';
import scissorIcon from '../images/scissor.svg';
import { GAME_HAND_SIGNS } from '../game/constants';

export default (sign, clickHandler) => {
  const handSign = document.createElement('img');
  handSign.setAttribute('data-sign', sign);
  switch (sign) {
    case GAME_HAND_SIGNS.ROCK:
      handSign.src = rockIcon;
      break;

    case GAME_HAND_SIGNS.PAPER:
      handSign.src = paperIcon;
      break;

    case GAME_HAND_SIGNS.SCISSOR:
      handSign.src = scissorIcon;
      break;

    default:
      handSign.src = '';
      break;
  }
  if (clickHandler) {
    handSign.classList.add('hand-sign');
    handSign.addEventListener('click', clickHandler);
  }

  return handSign;
};
