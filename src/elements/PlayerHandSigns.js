import HandSign from './HandSign';
import { GAME_EVENTS, GAME_HAND_SIGNS } from '../game/constants';

const playerHandSigns = (parentElm, gameController) => {
  const gameHandSigns = document.createElement('div');
  gameHandSigns.classList.add('hand-sign-wrapper');

  Object.getOwnPropertyNames(GAME_HAND_SIGNS)
    .map(sign => HandSign(sign, () => {
      gameController.updateGameState({
        type: GAME_EVENTS.SELECT_HAND_SIGN,
        payload: {
          playerOneHandSign: GAME_HAND_SIGNS[sign],
          playerTwoHandSign: null
        }
      });
    }))
    .forEach((sign) => {
      gameHandSigns.appendChild(sign);
    });
  parentElm.appendChild(gameHandSigns);
};

export default playerHandSigns;
