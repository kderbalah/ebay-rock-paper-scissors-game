import GameController from './GameController';

export default class Game {
  constructor(initialState, handSigns, rules) {
    this.gameController = new GameController(initialState, handSigns, rules);
  }

  getGameController() {
    return this.gameController;
  }
}
