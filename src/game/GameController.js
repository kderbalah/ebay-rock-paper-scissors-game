import GameStateManager from './GameStateManager';
import { GAME_EVENTS, GAME_STATUS } from './constants';

export default class GameController {
  constructor(initialState, handSigns, rules) {
    this.initialGameState = initialState;
    this.gameHandSigns = handSigns;
    this.gameRules = rules;
    this.gameStateManager = new GameStateManager(this.gameStateReducer);
  }

  gameStateReducer = (state = this.initialGameState, action) => {
    switch (action.type) {
      case GAME_EVENTS.SELECT_HAND_SIGN: {
        const { playerOneHandSign, playerTwoHandSign } = action.payload;
        const handOne = playerOneHandSign || this.randomHandSign();
        const handTwo = playerTwoHandSign || this.randomHandSign();

        const newScore = this.judge(handOne, handTwo);
        return Object.assign({}, state, {
          score: {
            PLAYER_ONE_SCORE:
              state.score.PLAYER_ONE_SCORE + newScore.score.PLAYER_ONE_SCORE,
            PLAYER_TWO_SCORE:
              state.score.PLAYER_TWO_SCORE + newScore.score.PLAYER_TWO_SCORE
          },
          status: newScore.status,
          playerSign: handOne,
          opponentSign: handTwo
        });
      }
      case GAME_EVENTS.RESET_GAME:
        return this.initialGameState;
      default:
        return state;
    }
  };

  randomHandSign() {
    const signs = Object.keys(this.gameHandSigns);
    return signs[Math.floor(Math.random() * signs.length)];
  }

  judge(playerOneSign, playerTwoSign) {
    if (playerOneSign === playerTwoSign) {
      return {
        score: {
          PLAYER_ONE_SCORE: 0,
          PLAYER_TWO_SCORE: 0
        },
        status: GAME_STATUS.TIE
      };
    }

    const playerOneWins = this.handSignWins(playerOneSign, playerTwoSign);
    return {
      score: {
        PLAYER_ONE_SCORE: playerOneWins ? 1 : 0,
        PLAYER_TWO_SCORE: playerOneWins ? 0 : 1
      },
      status: playerOneWins ? GAME_STATUS.WIN : GAME_STATUS.LOSE
    };
  }

  handSignWins(sign, opponentSign) {
    return this.gameRules[sign].defeat.indexOf(opponentSign) >= 0;
  }

  subscribeToGameUpdate(gameHandler) {
    return this.gameStateManager.subscribeToGameStateUpdate(gameHandler);
  }

  getGameState() {
    return this.gameStateManager.getGameState();
  }

  updateGameState(gameAction) {
    return this.gameStateManager.updateGameState(gameAction);
  }
}
