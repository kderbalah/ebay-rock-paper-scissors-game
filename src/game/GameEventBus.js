export default class GameEventBus {
  constructor() {
    this.gameEvents = {};
  }

  getGameEvents() {
    return this.gameEvents;
  }

  addGameEventListener(event, eventHandler) {
    this.gameEvents[event] = this.gameEvents[event] || [];
    this.gameEvents[event].push(eventHandler);
  }

  removeGameEventListener(event, eventHandler) {
    if (this.gameEvents[event]) {
      for (let i = 0; i < this.gameEvents[event].length; i += 1) {
        if (this.gameEvents[event][i] === eventHandler) {
          this.gameEvents[event].splice(i, 1);
          break;
        }
      }
    }
  }

  emitGameEvent(event, payload) {
    if (this.gameEvents[event]) {
      this.gameEvents[event].forEach((eventHandler) => {
        eventHandler(payload);
      });
    }
  }
}
