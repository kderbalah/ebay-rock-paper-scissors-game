import GameEventBus from './GameEventBus';
import { GAME_EVENTS } from './constants';

let instance = null;

export default class GameStateManager {
  constructor(gameReducer) {
    if (!instance) {
      instance = this;
    }
    this.gameReducer = gameReducer;
    this.gameEventBus = new GameEventBus();
    this.updateGameState({});
    return instance;
  }

  getGameState() {
    return this.gameState;
  }

  updateGameState(gameAction) {
    this.gameState = this.gameReducer(this.gameState, gameAction);
    this.gameEventBus.emitGameEvent(GAME_EVENTS.UPDATE_GAME_STATE);
  }

  subscribeToGameStateUpdate(handler) {
    this.gameEventBus.addGameEventListener(
      GAME_EVENTS.UPDATE_GAME_STATE,
      handler
    );
    return {
      unsubscribe: () => {
        this.unsubscribeFromGameStateUpdate(handler);
      }
    };
  }

  unsubscribeFromGameStateUpdate(handler) {
    this.gameEventBus.removeGameEventListener(
      GAME_EVENTS.UPDATE_GAME_STATE,
      handler
    );
  }
}
