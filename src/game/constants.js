export const GAME_INITIAL_STATE = {
  score: {
    PLAYER_ONE_SCORE: 0,
    PLAYER_TWO_SCORE: 0
  },
  status: null,
  playerSign: null,
  opponentSign: null
};

export const GAME_MODES = {
  PLAYER_VS_COMP: 'PLAYER_VS_COMP',
  COMP_VS_COMP: 'COMP_VS_COMP'
};

export const GAME_STATUS = {
  WIN: 'WIN',
  LOSE: 'LOSE',
  TIE: 'TIE'
};

export const GAME_EVENTS = {
  SELECT_HAND_SIGN: 'SELECT_HAND_SIGN',
  RESET_GAME: 'RESET_GAME',
  UPDATE_GAME_STATE: 'UPDATE_GAME_STATE'
};

export const GAME_HAND_SIGNS = {
  ROCK: 'ROCK',
  PAPER: 'PAPER',
  SCISSOR: 'SCISSOR'
};

const RULES = {};

RULES[GAME_HAND_SIGNS.ROCK] = {
  defeat: [GAME_HAND_SIGNS.SCISSOR]
};

RULES[GAME_HAND_SIGNS.SCISSOR] = {
  defeat: [GAME_HAND_SIGNS.PAPER]
};

RULES[GAME_HAND_SIGNS.PAPER] = {
  defeat: [GAME_HAND_SIGNS.ROCK]
};

export const GAME_RULES = RULES;
