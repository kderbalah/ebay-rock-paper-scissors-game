import Game from './game/Game';
import {
  GAME_INITIAL_STATE,
  GAME_HAND_SIGNS,
  GAME_RULES,
  GAME_EVENTS
} from './game/constants';
import HandSign from './elements/HandSign';
import PlayerHandSigns from './elements/PlayerHandSigns';

import './styles.css';

const game = new Game(GAME_INITIAL_STATE, GAME_HAND_SIGNS, GAME_RULES);
const gameController = game.getGameController();

const renderGame = () => {
  const scoreElm = document.querySelector('#score');
  const {
    score,
    status,
    playerSign,
    opponentSign
  } = gameController.getGameState();
  scoreElm.innerHTML = `${score.PLAYER_ONE_SCORE} - ${score.PLAYER_TWO_SCORE} `;

  const resetElm = document.querySelector('#reset a');
  resetElm.addEventListener('click', (e) => {
    e.preventDefault();
    gameController.updateGameState({
      type: GAME_EVENTS.RESET_GAME
    });
  });

  if (status) {
    const handSigns = document.querySelectorAll('.hand-sign');
    Array.prototype.forEach.call(handSigns, (sign) => {
      if (sign.getAttribute('data-sign') === playerSign) sign.classList.toggle('selected', true);
      else sign.classList.toggle('disabled', true);
    });

    const opponentElm = document.querySelector('#opponent');
    const opponent = HandSign(opponentSign);
    opponentElm.appendChild(opponent);

    const statusElm = document.querySelector('#status');
    statusElm.innerHTML = `${status}!`;
    statusElm.className = 'slideDown';

    setTimeout(() => {
      statusElm.className = '';
      Array.prototype.forEach.call(handSigns, (sign) => {
        if (sign.getAttribute('data-sign') === playerSign) sign.classList.toggle('selected', false);
        else sign.classList.toggle('disabled', false);
      });
      opponentElm.innerHTML = '';
    }, 1500);
  }
};

const computerVsComputerRound = () => {
  const computerMode = document.createElement('div');
  computerMode.setAttribute('id', 'computer-mode');
  const modeLink = document.createElement('a');
  modeLink.href = '';
  modeLink.innerHTML = 'COMPUTER vs. COMPUTER<br><small>Let the computer play for you! 🤖</small>';
  modeLink.addEventListener('click', (e) => {
    e.preventDefault();
    gameController.updateGameState({
      type: GAME_EVENTS.SELECT_HAND_SIGN,
      payload: {
        playerOneHandSign: null,
        playerTwoHandSign: null
      }
    });
  });
  computerMode.appendChild(modeLink);
  return computerMode;
};

gameController.subscribeToGameUpdate(renderGame);
renderGame();

const gameContainer = document.querySelector('#game');
PlayerHandSigns(gameContainer, gameController);
gameContainer.appendChild(computerVsComputerRound());
