import expect from 'expect';
import Game from '../src/game/Game';
import {
  GAME_INITIAL_STATE,
  GAME_HAND_SIGNS,
  GAME_RULES,
  GAME_EVENTS
} from '../src/game/constants';

describe('Rock Paper Scissors Game', () => {
  let game;
  let gameController;

  before(() => {
    game = new Game(GAME_INITIAL_STATE, GAME_HAND_SIGNS, GAME_RULES);
    gameController = game.getGameController();
  });

  after(() => {});

  it('New game state should be the default initial state', () => {
    const gameState = gameController.getGameState();
    expect(gameState).toBe(GAME_INITIAL_STATE);
  });

  it('Reset action replaces game state with initial state', () => {
    const gameState = {
      score: {
        PLAYER_ONE_SCORE: 10,
        PLAYER_TWO_SCORE: 5
      },
      status: null,
      playerSign: null,
      opponentSign: null
    };

    const resetAction = {
      type: GAME_EVENTS.RESET_GAME
    };

    const newGameState = gameController.gameStateReducer(
      gameState,
      resetAction
    );

    const { score } = newGameState;
    expect(score.PLAYER_ONE_SCORE).toBe(0);
    expect(score.PLAYER_TWO_SCORE).toBe(0);
  });

  describe('Game reducer', () => {
    it('Tie if players selected same hand sign', () => {
      const action = {
        type: GAME_EVENTS.SELECT_HAND_SIGN,
        payload: {
          playerOneHandSign: GAME_HAND_SIGNS.ROCK,
          playerTwoHandSign: GAME_HAND_SIGNS.ROCK
        }
      };
      const newGameState = gameController.gameStateReducer(
        GAME_INITIAL_STATE,
        action
      );
      const { score } = newGameState;
      expect(score.PLAYER_ONE_SCORE).toBe(0);
      expect(score.PLAYER_TWO_SCORE).toBe(0);
    });

    describe('Rock', () => {
      it('should win the game against scissor', () => {
        const action = {
          type: GAME_EVENTS.SELECT_HAND_SIGN,
          payload: {
            playerOneHandSign: GAME_HAND_SIGNS.ROCK,
            playerTwoHandSign: GAME_HAND_SIGNS.SCISSOR
          }
        };
        const newGameState = gameController.gameStateReducer(
          GAME_INITIAL_STATE,
          action
        );
        const { score } = newGameState;
        expect(score.PLAYER_ONE_SCORE).toBe(1);
        expect(score.PLAYER_TWO_SCORE).toBe(0);
      });

      it('should lose the game against paper', () => {
        const action = {
          type: GAME_EVENTS.SELECT_HAND_SIGN,
          payload: {
            playerOneHandSign: GAME_HAND_SIGNS.ROCK,
            playerTwoHandSign: GAME_HAND_SIGNS.PAPER
          }
        };
        const newGameState = gameController.gameStateReducer(
          GAME_INITIAL_STATE,
          action
        );
        const { score } = newGameState;
        expect(score.PLAYER_ONE_SCORE).toBe(0);
        expect(score.PLAYER_TWO_SCORE).toBe(1);
      });
    });

    describe('Paper', () => {
      it('should win the game against rock', () => {
        const action = {
          type: GAME_EVENTS.SELECT_HAND_SIGN,
          payload: {
            playerOneHandSign: GAME_HAND_SIGNS.PAPER,
            playerTwoHandSign: GAME_HAND_SIGNS.ROCK
          }
        };
        const newGameState = gameController.gameStateReducer(
          GAME_INITIAL_STATE,
          action
        );
        const { score } = newGameState;
        expect(score.PLAYER_ONE_SCORE).toBe(1);
        expect(score.PLAYER_TWO_SCORE).toBe(0);
      });

      it('should lose the game against scissors', () => {
        const action = {
          type: GAME_EVENTS.SELECT_HAND_SIGN,
          payload: {
            playerOneHandSign: GAME_HAND_SIGNS.PAPER,
            playerTwoHandSign: GAME_HAND_SIGNS.SCISSOR
          }
        };
        const newGameState = gameController.gameStateReducer(
          GAME_INITIAL_STATE,
          action
        );
        const { score } = newGameState;
        expect(score.PLAYER_ONE_SCORE).toBe(0);
        expect(score.PLAYER_TWO_SCORE).toBe(1);
      });
    });

    describe('Scissors', () => {
      it('should win the game against paper', () => {
        const action = {
          type: GAME_EVENTS.SELECT_HAND_SIGN,
          payload: {
            playerOneHandSign: GAME_HAND_SIGNS.SCISSOR,
            playerTwoHandSign: GAME_HAND_SIGNS.PAPER
          }
        };
        const newGameState = gameController.gameStateReducer(
          GAME_INITIAL_STATE,
          action
        );
        const { score } = newGameState;
        expect(score.PLAYER_ONE_SCORE).toBe(1);
        expect(score.PLAYER_TWO_SCORE).toBe(0);
      });

      it('should lose the game against rock', () => {
        const action = {
          type: GAME_EVENTS.SELECT_HAND_SIGN,
          payload: {
            playerOneHandSign: GAME_HAND_SIGNS.SCISSOR,
            playerTwoHandSign: GAME_HAND_SIGNS.ROCK
          }
        };
        const newGameState = gameController.gameStateReducer(
          GAME_INITIAL_STATE,
          action
        );
        const { score } = newGameState;
        expect(score.PLAYER_ONE_SCORE).toBe(0);
        expect(score.PLAYER_TWO_SCORE).toBe(1);
      });
    });
  });
});
