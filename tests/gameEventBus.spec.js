import expect, { createSpy } from 'expect';
import GameEventBus from '../src/game/GameEventBus';

describe('Game Event Bus', () => {
  let eventBus;

  const TEST_EVENT = 'TEST_EVENT';
  const eventHandler = function eventHandlerOne() {};
  const anotherEventHandler = function eventHandlerTwo() {};

  const SPY_EVENT = 'SPY_EVENT';
  const spyHandler = createSpy();

  before(() => {
    eventBus = new GameEventBus();
  });

  it('Add game event listener', () => {
    eventBus.addGameEventListener(TEST_EVENT, eventHandler);
    const events = eventBus.getGameEvents();
    expect(events[TEST_EVENT].length).toBe(1);
    expect(events[TEST_EVENT][0]).toBe(eventHandler);
  });

  it('Remove game event listener', () => {
    eventBus.removeGameEventListener(TEST_EVENT, eventHandler);
    const events = eventBus.getGameEvents();
    expect(events[TEST_EVENT].length).toBe(0);
  });

  it('Multiple game event listeners', () => {
    eventBus.addGameEventListener(TEST_EVENT, eventHandler);
    eventBus.addGameEventListener(TEST_EVENT, anotherEventHandler);
    const events = eventBus.getGameEvents();
    expect(events[TEST_EVENT].length).toBe(2);
  });

  it('Emit game event', () => {
    eventBus.addGameEventListener(SPY_EVENT, spyHandler);
    eventBus.emitGameEvent(SPY_EVENT, {});
    expect(spyHandler).toHaveBeenCalled();
  });
});
